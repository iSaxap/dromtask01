PROJECT_DIRS=./src ./bin
PHP_CS_FIXER_CONFIG=--config=php-cs-fixer.php

csfix:
	./vendor/bin/php-cs-fixer fix ${PHP_CS_FIXER_CONFIG} -- ${PROJECT_DIRS}

stan:
	./vendor/bin/phpstan analyse ${PROJECT_DIRS}