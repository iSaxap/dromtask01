<?php

use App\Classes\Exceptions\InvalidCommandException;
use App\Classes\Interfaces\CommandInterface;

require 'vendor/autoload.php';

try {
    unset($argv[0]);
    $command = array_shift($argv);
    $className = '\\App\\Commands\\' . $command;

    // Подготавливаем список аргументов
    $params = [];
    foreach ($argv as $argument) {
        preg_match('/^-(.+)=(.+)$/', $argument, $matches);
        if (!empty($matches)) {
            $paramName = $matches[1];
            $paramValue = $matches[2];

            $params[$paramName] = $paramValue;
        }
    }

    $startTime = microtime(true);

    // Создаём экземпляр класса, передав параметры и вызываем метод execute()
    $class = new $className($params);
    if (!$class instanceof CommandInterface) {
        throw new InvalidCommandException("Комманда ${command} не может быть выполнена");
    }
    $class->execute();
    echo "Время выполнения = \033[34m" . (microtime(true) - $startTime) . "\033[0m" . PHP_EOL;
    echo "Пиковое потребление памяти \033[34m" . round(memory_get_peak_usage()/(1024*1024), 4) . " Mb \033[0m" . PHP_EOL;
} catch (\Exception $e) {
    echo 'Error: ' . $e->getMessage(); 
}