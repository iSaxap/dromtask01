<?php

namespace App\Classes;

class GenerateSettingsHandbook
{
    public const INVALID_CONTENT = "invalidValue";

    // Файл который считаем
    public const TARGET_FILENAME = 'count';
    // Вероятность создания не валидного файла
    public const CHANCE_INVALID_CONTENT = 10;
    // Вероятность создания файла
    public const CHANCE_VALID_FILE = 50;
    // Вероятность создания вложенности
    public const CHANCE_IN = 50;
    // Максимальное количество файлов в директории
    public const MAX_COUNT_FILES_IN_DIR = 10;
    // Максимальное количество директорий в директории
    public const MAX_COUNT_DIRS_IN_LEVEL = 5;
    // Максимальная вложеность
    public const MAX_LEVEL = 20;
}
