<?php

namespace App\Classes\Interfaces;

interface CommandInterface
{
    public function execute(): void;
}
