<?php

namespace App\Commands;


use App\Classes\Exceptions\InvalidParamsException;

abstract class AbstractDirectoryCommand
{
    protected string $path;
    private array $params;

    /**
     * @throws InvalidParamsException
     */
    public function __construct(array $params)
    {
        $this->path = $params['path'] ?? $this->getDefaultPath();
        unset($params['path']);
        $this->validatePath($this->path);
        $this->params = $params;
    }

    /**
     * @throws InvalidParamsException
     */
    protected function validatePath(string $path): void
    {
        if (!is_dir($path)) {
            throw new InvalidParamsException("Path: {$path} is not valid");
        };
    }

    /**
     * @return mixed
     */
    protected function getParam(string $nameParam)
    {
        return $this->params[$nameParam] ?? null;
    }

    private function getDefaultPath(): string
    {
        $basePath = __DIR__ . '/../../source';
        if (!is_dir($basePath)) {
            mkdir($basePath);
        }
        return $basePath;
    }
}
