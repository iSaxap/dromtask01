<?php

namespace App\Commands;

use App\Classes\Interfaces\CommandInterface;

class MapCalculate extends AbstractDirectoryCommand implements CommandInterface
{
    private $cache;

    public function execute(): void
    {
        $result = $this->mapExecutor();

        echo "Результат = " . $result . PHP_EOL;
    }

    private function mapExecutor(): string
    {
        $cachePath = $this->path . '/cache-map';
        if ($this->getParam('cache') === 'reindex' && file_exists($cachePath)) {
            unlink($cachePath);
        }

        if (!file_exists($cachePath)) {
            $this->cache = fopen($cachePath, "a+");
            $this->writeMapFiles($this->path);
            fclose($this->cache);
        }

        return $this->calculate();
    }

    private function readFile($path): \Generator
    {
        $handle = fopen($path, "r");
        while(!feof($handle)) {
            yield trim(fgets($handle));
        }
        fclose($handle);
    }

    private function writeMapFiles(string $dir): void
    {
        $files = scandir($dir);

        foreach ($files as $item) {
            $path = realpath($dir . '/' . $item);
            if (preg_match('|\/count.*|', $path)) {
                 fwrite($this->cache, $path . PHP_EOL);
            } else if (is_dir($path) && $item != "." && $item != "..") {
                $this->writeMapFiles($path);
            }
        }
    }

    private function calculate(): string
    {
        $sum = '0';
        $cache = $this->readFile($this->path . '/cache-map');

        foreach ($cache as $row) {
            if (empty($row) || !file_exists($row)) {
                continue;
            }
            $content = file_get_contents($row);
            if (!is_numeric($content)) {
                continue;
            }
            $sum = bcadd($sum, $content);
        }
        return $sum;
    }

}