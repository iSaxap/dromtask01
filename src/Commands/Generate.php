<?php

namespace App\Commands;

use App\Classes\GenerateSettingsHandbook;
use App\Classes\Interfaces\CommandInterface;

class Generate extends AbstractDirectoryCommand implements CommandInterface
{
    private array $map = [];
    private string $sum = '0';

    public function execute(): void
    {
        if (filter_var($this->getParam('clear'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
            $this->clearDir($this->path);
            echo "\033[41m Директория очищена \033[0m" . PHP_EOL;
        }

        $this->map[] = $this->path;

        $this->generateStuff();

        $this->calculateGenerateSum();
    }

    private function generateStuff(): void
    {
        while (!empty($this->map)) {
            $path = array_shift($this->map);

            $random = rand(0, 100);

            if ($path === $this->path || (GenerateSettingsHandbook::CHANCE_IN >= $random && GenerateSettingsHandbook::MAX_LEVEL > $this->pathLevel($path))) {
                $this->generateLevel($path);
            }
        }
    }

    private function generateLevel(string $basePath): void
    {
        $countDirs = rand(1, GenerateSettingsHandbook::MAX_COUNT_DIRS_IN_LEVEL);

        for ($i = 1; $i <= $countDirs; $i++) {
            $this->map[] = $this->createDir($basePath);
        }
    }

    private function createDir(string $path): string
    {
        $dirPath = $path . '/' . uniqid();

        if (!is_dir($dirPath)) {
            mkdir($dirPath);
            $this->generateFiles($dirPath);
        }
        return $dirPath;
    }

    private function generateFiles(string $dirPath): void
    {
        $countFiles = rand(0, GenerateSettingsHandbook::MAX_COUNT_FILES_IN_DIR);
        for ($i = 1; $i <= $countFiles; $i++) {
            $this->createFile($dirPath);
        }
    }

    private function createFile(string $dirPath): void
    {
        $random = rand(0, 100);
        $fileName = GenerateSettingsHandbook::TARGET_FILENAME . uniqid();
        $content = '';

        if (GenerateSettingsHandbook::CHANCE_VALID_FILE >= $random) {
            $content = $this->getContent();
        } elseif (100 - GenerateSettingsHandbook::CHANCE_INVALID_CONTENT <= $random) {
            $content = GenerateSettingsHandbook::INVALID_CONTENT;
        } else {
            $fileName = uniqid();
        }

        file_put_contents($dirPath . '/' . $fileName, $content);
    }

    private function getContent(): int
    {
        $content = rand(0, PHP_INT_MAX);
        $this->sum = bcadd($this->sum, $content);

        return $content;
    }

    private function clearDir(string $path): void
    {
        if (is_file($path)) {
            unlink($path);
        } elseif (is_dir($path)) {
            foreach (scandir($path) as $p) {
                if (($p!='.') && ($p!='..')) {
                    $this->clearDir($path . '/' . $p);
                }
            }
            if ($path !== $this->path) {
                rmdir($path);
            }
        }
    }

    private function calculateGenerateSum(): void
    {
        $beforeSum = file_exists($this->path . '/sum') ? file_get_contents($this->path . '/sum') : 0;
        $afterSum = is_numeric($beforeSum) ? bcadd($beforeSum, $this->sum) : 0;
        file_put_contents($this->path . '/sum', $afterSum);

        echo 'Сумма чисел в сгенерированных файлах = ' . $this->sum . PHP_EOL;
        echo 'Сумма чисел предыдущих генераций файлов = ' . $beforeSum . PHP_EOL;
        echo 'Сумма чисел всех генераций файлов = ' . $afterSum . PHP_EOL;
    }

    private function pathLevel(string $path): int
    {
        return substr_count($path, '/') - (substr_count($this->path, '/'));
    }
}
