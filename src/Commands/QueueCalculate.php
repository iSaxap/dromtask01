<?php

namespace App\Commands;

use App\Classes\Interfaces\CommandInterface;

class QueueCalculate extends AbstractDirectoryCommand implements CommandInterface
{
    public function execute(): void
    {
        $result = $this->queueExecutor();

        echo "Результат = " . $result . PHP_EOL;
    }

    private function queueExecutor(): string
    {
        return $this->queueCalculate();
    }

    private function queueCalculate(): string
    {
        $stack[] = realpath($this->path);

        $results = '0';
        while (!empty($stack)) {
            $dir = array_shift($stack);
            $dirs = scandir($dir);
            foreach ($dirs as $item) {
                $path = $dir . '/' . $item;
                if (!is_dir($path) && preg_match('|count.*|', $path) === 1) {
                    $content = file_get_contents($path);
                    if (is_numeric($content)) {
                        $results = bcadd($results, $content);
                    }
                } else if (is_dir($path) && $item != "." && $item != "..") {
                    $stack[] = $path;
                }
            }
        }
        return $results;
    }
}