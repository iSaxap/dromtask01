<?php

namespace App\Commands;

use App\Classes\Interfaces\CommandInterface;

class StackCalculate extends AbstractDirectoryCommand implements CommandInterface
{
    public function execute(): void
    {
        $result = $this->stackExecutor();

        echo "Результат = " . $result . PHP_EOL;
    }

    private function stackExecutor(): string
    {
        return $this->stackCalculate();
    }

    private function stackCalculate(): string
    {
        $stack[] = realpath($this->path);

        $results = '0';
        while (!empty($stack)) {
            $dir = array_pop($stack);
            $dirs = scandir($dir);
            foreach ($dirs as $item) {
                $path = $dir . '/' . $item;
                if (!is_dir($path) && preg_match('|count.*|', $path) === 1) {
                    $content = file_get_contents($path);
                    if (is_numeric($content)) {
                        $results = bcadd($results, $content);
                    }
                } else if (is_dir($path) && $item != "." && $item != "..") {
                    $stack[] = $path;
                }
            }
        }
        return $results;
    }
}