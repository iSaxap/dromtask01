<?php

namespace App\Commands;

use App\Classes\Interfaces\CommandInterface;

class RecursiveCalculate extends AbstractDirectoryCommand implements CommandInterface
{
    public function execute(): void
    {

        $result = $this->recursiveExecutor();

        echo "Результат = " . $result . PHP_EOL;
    }

    private function recursiveExecutor(): string
    {
        return $this->recursiveCalculate($this->path);
    }

    private function recursiveCalculate(string $dir, string &$results = '0'): string
    {
        $files = scandir($dir);

        foreach ($files as $item) {
            $path = realpath($dir . '/' . $item);
            if (!is_dir($path)) {
                if (preg_match('|count.*|', $item) === 1) {
                     $content = file_get_contents($path);
                     if (is_numeric($content)) {
                         $results = bcadd($results, $content);
                     }
                }
            } else if ($item != "." && $item != "..") {
                $this->recursiveCalculate($path, $results);
            }
        }

        return $results;
    }
}